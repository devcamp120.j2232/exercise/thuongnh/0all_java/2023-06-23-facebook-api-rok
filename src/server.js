import express from "express";
import viewEngine from "./config/viewEngine";
import initWebRoute from "./routes/web";
import bodyParser from "body-parser";
require("dotenv").config();


let app = express();

// cấuHìnhiew engine   
viewEngine(app);

// parse request to json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended:   true }));

//init web route
initWebRoute(app);
let PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;
console.log("token ở file server : " + PAGE_ACCESS_TOKEN);
let port = process.env.PORT || 8081;
console.log("chát box đang chạy ở cổng thử nghiệm " + port);
app.listen(port, () => {
    console.log("chát box đang chạy ở cổng " + port);
});



app.listen();