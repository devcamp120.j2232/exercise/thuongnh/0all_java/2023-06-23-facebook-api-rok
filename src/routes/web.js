import express from "express";
import chatboxController from "../controller/chatboxController";

let router = express.Router();

let initWebRoutes = (app)=> {
    router.get("/", chatboxController.getHomePage);
    // api facebook
    router.get("/webhook", chatboxController.getWebhook);
    router.post("/webhook", chatboxController.postWebhook);


    return app.use("/", router);
};

module.exports = initWebRoutes;